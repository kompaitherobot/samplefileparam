import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

   filetext : string;

  constructor(public navCtrl: NavController,public file: File) {


  }



  createNewFolder(path,nameFolder){

    //check folder exist
    this.file.checkDir(this.file.dataDirectory, 'mydir').then(_ => {
      //create folder
      console.log('Directory exists');

    }).catch(err =>{
      console.log("Directory doesn't exist");
      let result = this.file.createDir(path,nameFolder,true);
      console.log("director created");
    });
  }

  createNewFile(path,nameFile,obj){



    this.file.checkFile(path,nameFile).then(_=>{

    }).catch(error=>{
      // file no exist
      this.file.writeFile(path,nameFile,JSON.stringify(obj),{replace:true});

      console.log("file created");
    });

  }

  getTextFile(path,nameFile){

    var prjson = this.file.readAsText(path,nameFile);
    prjson.then(data=>{
      this.filetext = JSON.parse(data);
      console.log("file get");
    }).catch(error =>
    {
      console.log(error)
    });

  }

  removeFile(path,nameFile){



    this.file.checkFile(path,nameFile).then(_=>{
      // file exist

      this.file.removeFile(path,nameFile);

      console.log("file removed");

    }).catch(error=>{
      // file no exist
      console.log(error);
    });

  }

  removeFolder(path,nameFolder)
  {

    //check folder exist
    this.file.checkDir(path,nameFolder).then(_ => {
      //remove folder
      console.log('Directory exists');
      this.file.removeDir(path,nameFolder);
      console.log("folder removed");

    }).catch(err =>{
      console.log("Directory doesn't exist");
    });
  }

  modifFile(path,namefile,obj){

    this.file.checkFile(path,namefile).then(_=>{
      // file exist

      this.file.writeFile(path,namefile,JSON.stringify(obj),{replace:true});
      console.log("file modified");

      console.log("file removed");

    }).catch(error=>{
      // file no exist
      console.log(error);
    });


  }


  // Example
  createFile(){
    // check folder exist

    var text = '{ "employees" : [' +
      '{ "firstName":"John" , "lastName":"Doe" },' +
      '{ "firstName":"Anna" , "lastName":"Smith" },' +
      '{ "firstName":"Peter" , "lastName":"Jones" } ]}';

    var obj = JSON.parse(text);

    this.file.checkDir(this.file.dataDirectory, 'param').then(_ => {

      console.log('Directory exists');

      // getTextFile
      var prjson = this.file.readAsText(this.file.dataDirectory + "/mydir/","HW.json");
      prjson.then(data=>{
        var json = JSON.parse(data);
        json["employees"][0]["firstName"] = "Gontran";
        // modifFile
        this.file.writeFile(this.file.dataDirectory + "/mydir/","HW.json",JSON.stringify(json),{replace:true});
        console.log(json);
      }).catch(error =>
      {
        console.log(error)
      });


    }).catch(err =>{
      console.log("Directory doesn't exist");

      //create folder
      let result = this.file.createDir(this.file.dataDirectory,"mydir",true);

      result.then( data => {
        alert(" Dir created at" + data.toURL());

        //create file

        this.file.writeFile(data.toURL(),"HW.json",JSON.stringify(obj),{replace:true});

        alert("file created");


      }).catch(error =>{
        alert(error);
      })
    });

  }



}
